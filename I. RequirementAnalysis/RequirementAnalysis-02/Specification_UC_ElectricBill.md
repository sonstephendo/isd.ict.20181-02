# Use Case Electric Bill payment 
### Description
A customer can pay electric bill monthly via the Internet Banking System.

#
### Primary Actor
* Customer

#
### Secondary Actor
* EVN system software

#
### Preconditions
* The user successfully logged in to the system.

#
### Postconditions
* None

#
### Main flow
1. The user selects the *Pay Electric Bills* option in the system.
2. The user provides the *ID number* register with the Electric Service Provider. 
3. The system sends the ID number to EVN system software.
4. EVN system process the information and return customer's info and the amount of money needed to be paid for this month to bank system.
5. Display the result to user.
6. The user, if confirm to pay the bill, proceed to the next step to receive the OTP code to validate the action.
7. User enter the receipt OTP and confirm second time. 
8. The EVN system and bank system executes the payment and returns E-receipt result for the user.
#

### Alternative Flows
* **5a.** The user enters invalid *ID number* of the registry System Service.
    1. The system notifies the user that it can not detect the ID of the electric service's account.
    2. Return to step 2.
* **5b.** It is not time to pay the bill yet 
    1. The system notifies the user that the service has not charged the fee for this month yet.
    2. The process ends here.
* **5c.** The user already paid the bill 
    1. The system notifies the user that the fee was paid.
    2. The process ends here.
* **7a.** OTP code never arrives or enter wrong OTP
    1. User requests another OTP codes sent if he/she has to wait for a long time. 
    2. Return to step 7. 

