# Use Case Specification : Change Password

### Description
Allows a Customer/Bank Officer to change their account password.

### Actor
* Customer
* Bank Officer

### Preconditions
* The user must be log into system.

### Main flow
1. The system display a menu.
2. The user selects *Change password*.
3. The user have to enter their current password.
4. The user have to enter their new password.
5. The user have to re-enter the new password.
6. The system register the user's new password.

### Alternative Flows
* **3a.** Incorrect current password entered.
    1. The system display an error message.
    2. Return to step 1.
* **3b.** The user choose to cancel their password changing process.
    1. Return to step 1.
* **4a.** The user enter the same password as their old password.
    1. The system notify to the user that their new password is the same as their old one.
    2. Re-ask a new password.
* **4b.** The user choose to cancel their password changing process.
    1. Return to step 1.
* **5a.** 1st new password and 2nd new password do not match.
    1. The system display an error message.
    2. Return to step 4.
* **5b.** The user choose to cancel their password changing process.
    1. Return to step 1.

### Postconditions
* None