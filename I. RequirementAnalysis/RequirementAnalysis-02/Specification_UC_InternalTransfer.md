# Use Case Specification : External Transfer 

### Description
Allows a Customer to transfer money to an account which is in the same bank system.

### Actor
* Customer

### Preconditions
* The user must be logged into the system.

### Main flow
1. The system display a menu.
2. The user selects the *internal transfer* option in the system.
3. The user have to chose the account which must be sold and count him which must be credited
4. The user enter the amount that he/she wants to transfer.
5. The software sends one-time password to customer.
6. The user enter the OTP. 
7. The software add a new transaction history and decrease the transfer amount the customer account.
8. The software notifies successfully transfer. 

### Alternative Flows
* **3a.** The user choose to cancel the transfer of money.
    1. Return to step 1.
* **4a.** The user transfer money exceed the amount available in the account or the maximum amount can be transfered at one time.
    1. The system notifies the user does not have enough money (or exceed amount thresshold) to execute this transfer.
    2.  Ask an other amount of money . 
* **4b.** The user choose to cancel the transfer of money.
    1. Return to step 1.
* **6a.** The user enter invalid OTP 
    1. The system display an error message. 
    2. Return to step 2.
* **6b.** The user choose to cancel the transfer of money.
    1. Return to step 1.

### Postconditions
* None