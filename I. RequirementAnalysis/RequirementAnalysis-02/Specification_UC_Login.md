# Use Case Login
### Description
User (Customers or Bank officers in this case) have to log in before using any service or do any task. So *Log in* is the first task needed to be done. Specifically, users have to fill in their username (phone number or personal email may do as well), password (PIN code) for the system to check.  

#
### Primary Actor
* Customer
* Bank Officer

#
### Secondary Actor
* None

#
### Preconditions
* The user needs to sign up for a valid account
* The user has to provide correct personal information for the bank to manage account
#

### Postconditions
* None

#
### Main flow
1. The **User** fill in the two fields username and password and confirm to send the data.
2. Waiting for the system to authenticate information.
3. The system grants the user the right to access. 

#
### Alternative Flows
* **3a.** The user enters an invalid account information.
    1. The system notifies the user that the account information is wrong, ask to refill the form.
    2. Return to step 1.
    
* **3b.** The user has entered an invalid account 3 times.
    1. The system display to the user that they have exceeded the maximum login attempts.
    2. The system notifies its administrator.
    
* **3c.** The user forgets his/her username or password.
    1. The system directs the user to another window. 
    2. The user has to fill in his/her correct information (phone number/email/citizen ID) for the bank to validate 
    3. After validation, new password/username will be sent to the user via his/her email
    4. The user uses this new password to access the service 