# Use Case Specification : External Transfer 

### Description
Allows a Customer to transfer money to an account which is not in the same bank system.

### Actor
* Customer
* Bank Consortium

### Preconditions
* The user must be logged into the system.

### Main flow
1. The system display a menu.
2. The user selects the *external transfer* option in the system.
3. The user selects the destination bank and enter a destination account
4. The software ask the Bank consortium the account holder and bank branch of the destination account.
5. The user enter the amount that he/she wants to transfer.
6. The software sends one-time password to customer.
7. The user enter the OTP. 
8. The software asks the bank consortium to process the transfer to the destination account, add a new transaction history and decrease the transfer amount the customer account.
9. The software notifies successfully transfer. 

### Alternative Flows
* **3a.** The user enter invalid destination account informations.
    1. The system display an error message.
    2. Re-ask destination account informations.
* **3b.** The user choose to cancel the transfer of money.
    1. Return to step 1.
* **5a.** The user transfer money exceed the amount available in the account or the maximum amount can be transfered at one time.
    1. The system notifies the user does not have enough money (or exceed amount thresshold) to execute this transfer.
    2.  Ask an other amount of money . 
* **5b.** The user choose to cancel the transfer of money.
    1. Return to step 1.
* **7a.** The user enter invalid OTP 
    1. The system display an error message. 
    2. Return to step 2.
* **7b.** The user choose to cancel the transfer of money.
    1. Return to step 1.

### Postconditions
* None