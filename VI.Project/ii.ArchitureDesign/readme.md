Homework 05–Architectural pattern

Software architectural:

1. Frameworks:
   Backend: Laravel. (Programming language: PHP)
   Frontend: VueJs 2.(Programming language: HTML CSS Javascript)
2. Software architecture
   a. Origin architectural pattern: MVC pattern:
   ![alt text](./img/MVC.png 'MVC')
   b. Implementation
   ![alt text](./img/Laravel+Vuejs.png 'Laravel MVC mixs with Vue View')

   - Model: The model defines what data the app should contain.
   - Controller: The controller contains logic that updates the mode;
     and response to the view from the users of the app.

- View (implement with vuejs): The view defines how the app's data should be displayed.
  Component: An element of the view. It allows us to group the HTML code for reuse for the same module.
  View Store: The store containains share state of all component in the view. The state usually receive the data from "the controller" API in backend.
  Route: Manage the routing interface and switching page component to render the desired user's view.

Main flow:
![alt text](./img/Mainflow.png 'Main Flow')
