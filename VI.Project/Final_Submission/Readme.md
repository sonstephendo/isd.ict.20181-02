# Final submission (Group 2)
* Usecase's creator and review: Work.docx

## Documents
* Software Requirement Specification (SRS): SRS-Restaurant-Managerment-System.docx

* Software Design Description(SDD): SDD-Restaurant-Managerment-System.docx

* Test reports: Test_case_MyRestaurant.xlsx
## Source Code:
* Source Code: source/MyRestaurant

* Video: https://youtu.be/WnRfoSrmcoY

## Deploy:
http://myrestaurantg2.herokuapp.com/
* Admin account:
```
admin@gmail.com - admin123
```
* Kitchen account:
```
sonson@gmail.com - secret
```
